import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PushNotification from 'react-native-push-notification';

// import Header from './Header';

const { height } = Dimensions.get('window');

export default class Home extends Component {
  static navigationOptions = {
    drawerLabel: 'Trang chủ',
    // drawerIcon: () => (
    //   <Image
    //     source={require('./notif-icon.png')}
    //   />
    // ),
  };

  showNoti() {
    PushNotification.localNotification({
      /* iOS and Android properties */
      title: 'My Notification Title', // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
      message: 'My Notification Message', // (required)
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        {/* <Header /> */}

        <View >
          <View style={st.thanh1}>
            <View style={{ width: 35, height: 35, justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                <Image style={st.icon_thanh} source={require('../resources/icons-menu-filled.png')} />
              </TouchableOpacity>
            </View>
            <View style={st.thanhphai}>
              <TouchableOpacity onPress={() => navigate('Login')}>
                <Image style={st.icon_thanh} source={require('../img/large-icon-user.png')} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('Home')}>
                <Image style={st.icon_thanh} source={require('../img/home.png')} />
              </TouchableOpacity>
            </View>

          </View>
          <View style={st.thanh2}>
            <Image style={st.icon_logo} source={require('../img/logotayninh.png')} />
          </View>
          <View style={st.thanh2}>
            <Text style={{ color: 'blue', fontSize: 13, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
          </View>
          <View style={st.thanh2}>
            <Text style={{ color: 'blue', fontSize: 13, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
          </View>
        </View>
        <TouchableOpacity onPress={this.showNoti.bind(this)}>
        <Text>Touch</Text>
        </TouchableOpacity>
        <View >
          <View style={st.thanh2}>
            <TouchableOpacity >
              <Image style={st.icon} source={require('../img/ctl.png')} />
              <Text style={st.textDS}>Danh Sách</Text>
              <Text style={st.textDS2}>Cuộc Họp</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('MeetingSchedule')}>
              <Image style={st.icon} source={require('../img/calendar-icon.png')} />
              <Text style={st.textDS2}>  Lịch Họp</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('DayMeeting')}>
              <Image style={st.icon} source={require('../img/checklist-on-clipboard.png')} />
              <Text style={st.textDS}> Cuộc Họp</Text>
              <Text style={st.textDS}>Trong Ngày</Text>
            </TouchableOpacity>
          </View>
          <View style={st.thanh2} />
          <View style={st.thanh2}>
            <TouchableOpacity >
              <Image style={st.icon} source={require('../img/contact.png')} />
              <Text style={st.textDS}>Danh Sách</Text>
              <Text style={st.textDS2}>   Đơn Vị</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Image style={st.icon} source={require('../img/file.png')} />
              <Text style={st.textDS}>Cuộc Họp Mới</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Contact')}>
              <Image style={st.icon} source={require('../img/contact-icon.png')} />
              <Text style={st.textDS2}>Liên Hệ</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View >
          <View style={st.footer}>
            <Text >Copyright © Bản quyền thuộc </Text>
            <Text>Sở Thông tin và Truyền thông tỉnh Tây Ninh</Text>
          </View>
        </View>
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//     //   justifyContent: 'center',
//     //   alignItems: 'center',
//       backgroundColor: '#EB432D',
//     }
//   });
const st = StyleSheet.create({
  thanh1: {
    flexDirection: 'row',
    padding: 0,
    backgroundColor: 'red',
    borderBottomWidth: 1,
    // height: 55,
    justifyContent: 'space-between',
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    justifyContent: 'flex-end',
  },
  thanhtrai: {
    // flexDirection: 'row',
    // padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    // justifyContent: 'flex-end',
  },
  thanh2: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 0,
  },
  icon: {
    width: 75,
    height: 75,
    marginTop: 15,
    marginRight: 35,
    marginLeft: 25,
    marginBottom: 1
  },
  icon_thanh: {
    width: 35,
    height: 35,
    marginTop: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  icon_logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
    height: 120,
    width: 120,
  },
  textDS: {
    marginTop: 0,
    marginLeft: 30,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textDS2: {
    marginTop: 0,
    marginLeft: 32,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
