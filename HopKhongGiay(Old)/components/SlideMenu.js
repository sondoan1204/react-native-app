import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

import global from '../global';
import saveToken from '../api/saveToken';
import checkLogin from '../api/checkDangnhap';
import getToken from '../api/getToken';
import checkToken from '../api/checkToken';


export default class SlideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      id: {},
      active: 'Home',
    };
    global.onSignIn = this.onSignIn.bind(this);
    global.activescreen = this.resActive.bind(this);
  }
  componentDidMount() {
    getToken()
    .then(token => checkToken(token))
      .then(res => {
        if (res.thongbao === 'dung') {
          global.onSignIn(res);
          console.log('Kiem tra lai token dang nhap');
          console.log(res);
        }
      })
    .catch(err => console.log('LOI CHECK LOGIN', err)); 
  }
  onSignIn(id) {
    this.setState({ id });
    console.log('+++++++++++');
    console.log(id);
  }
  onSignOut() {
    this.setState({ id: null });
    this.setState({ active: 'Home' });
    saveToken('');
    this.props.navigation.navigate('Home');
    console.log('+++++++++++');
    console.log();
  }
  resActive(active) {
    this.setState({ active });
    console.log('___+++ Man Hinh Hien tai +++___');
    console.log(active);
  }
  render() {
    const { id } = this.state;
    const { navigate } = this.props.navigation;
    const viewbtnLogin = (
      <View>
      <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'Login' ? styles.active : {})]} onPress={() => { navigate('Login'); global.activescreen('Login'); }} >
        <Image style={styles.icon} source={require('../resources/icons-login.png')} />
        <Text style={styles.btnTextSignIn}>Đăng nhập</Text>
      </TouchableOpacity>
      </View>
    );
    const viewbtnUser = (
      <View>
        <Text> Tên:{id ? id.Ten : ''}</Text>
        <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'ChangePassword' ? styles.active : {})]} onPress={() => { navigate('ChangePassword'); global.activescreen('ChangePassword'); }}>
        <Image style={styles.icon} source={require('../resources/icons-password.png')} />
        <Text style={styles.btnTextSignIn}>Đổi mật khẩu</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnSignInStyle} onPress={this.onSignOut.bind(this)}>
        <Image style={styles.icon} source={require('../resources/icons-logout-rounded-up.png')} />
        <Text style={styles.btnTextSignIn}>Thoát</Text>
      </TouchableOpacity>
      </View>
    );
    const mainJSX = this.state.id ? viewbtnUser : viewbtnLogin;
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <Image
            source={require('../resources/logotayninh.png')}
            style={styles.profile}
          />
          <Text style={{ color: 'blue', fontSize: 8, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
          <Text style={{ color: 'blue', fontSize: 9, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
        </View>
        <View style={styles.loginContainer}>
          { mainJSX }
          <View style={styles.menuContainer}>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'Home' ? styles.active : {})]} onPress={() => { navigate('Home'); global.activescreen('Home'); }}>
              <Image style={styles.icon} source={require('../resources/icons-home.png')} />
              <Text style={styles.btnTextSignIn}>Trang chủ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'MeetingSchedule' ? styles.active : {})]} onPress={() => { navigate('MeetingSchedule'); global.activescreen('MeetingSchedule'); }}>
              <Image style={styles.icon} source={require('../resources/icons-calendar.png')} />
              <Text style={styles.btnTextSignIn}>Lịch họp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'DayMeeting' ? styles.active : {})]} onPress={() => { navigate('DayMeeting'); global.activescreen('DayMeeting'); }}>
              <Image style={styles.icon} source={require('../resources/icons8-today.png')} />
              <Text style={styles.btnTextSignIn}>Cuộc họp trong ngày</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'Search' ? styles.active : {})]} onPress={() => { navigate('Search'); global.activescreen('Search'); }}>
              <Image style={styles.icon} source={require('../resources/icons8-new.png')} />
              <Text style={styles.btnTextSignIn}>Tất cả cuộc họp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'MeetingSchedule' ? styles.active : {})]} onPress={() => { navigate('MeetingSchedule'); global.activescreen('DayMeeting'); }}>
              <Image style={styles.icon} source={require('../resources/icons8-checklist.png')} />
              <Text style={styles.btnTextSignIn}>Danh sách đơn vị</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnSignInStyle, (this.state.active === 'Contact' ? styles.active : {})]} onPress={() => { navigate('Contact'); global.activescreen('Contact'); }}>
              <Image style={styles.icon} source={require('../resources/icons-contact.png')} />
              <Text style={styles.btnTextSignIn}>Liên hệ</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D1D7E6',
    borderRightWidth: 3,
    borderColor: '#fff',
    alignItems: 'center'
  },
  profile: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginVertical: 15
  },
  btnStyle: {
    height: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: 70
  },
  btnText: {
    color: '#183883',
    fontFamily: 'Avenir',
    fontSize: 20
  },
  btnSignInStyle: {
    flexDirection: 'row',
    height: 30,
    backgroundColor: '#fff',
    borderRadius: 5,
    width: 200,
    marginBottom: 10,
    //justifyContent: 'space-between',
    paddingLeft: 10,
    alignItems: 'center'
  },
  active: {
    flexDirection: 'row',
    height: 30,
    backgroundColor: '#507ADE',
    borderRadius: 5,
    width: 200,
    marginBottom: 10,
    //justifyContent: 'space-between',
    paddingLeft: 10,
    alignItems: 'center'
  },
  btnTextSignIn: {
    color: '#183883',
    fontSize: 15,
    textAlign: 'center',
    marginLeft: 15
  },
  loginContainer: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  topContainer: {
    flex: 1,
    alignItems: 'center'
  },
  menuContainer: {
    marginTop: 30,
    flex: 1,
    alignItems: 'center'
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  username: {
    color: '#fff',
    fontFamily: 'Avenir',
    fontSize: 15,
  },
  icon: {
    width: 25,
    height: 25,
  }
});

AppRegistry.registerComponent('menu', () => SlideMenu);

// import React, { Component } from 'react';
// import { AppRegistry, View, Text, StyleSheet
// , Button, TouchableOpacity, StatusBar, Image } from 'react-native';

// export default class menu extends Component {
// static navigationOptions= ({ navigation }) => ({
// 		  title: 'Side Menu',	
// 	});  

// 	render() {
// 		const { navigate } = this.props.navigation;
// 		return (
// 	  <View >	

//       <Text style={styles.pageName}>Menu </Text>
// 	  <Button
// 	  onPress={() => navigate('MeetingSchedule')}
// 	  color="orange"
// 	  title="Electronics"
// 	  />

// 	    <Button
// 	  onPress={() => navigate('Products', { cat: 'Books', id: '4' })}
// 	  color="red"
// 	  title="Books"
// 	    />
//       </View>
// 		);
// 	}
// }
// const styles = StyleSheet.create({
// 	pageName: {
// 		margin: 10, 
// fontWeight: 'bold',
// 		color: '#000',
// textAlign: 'center'
// 	},
// });


// AppRegistry.registerComponent('menu', () => menu);
