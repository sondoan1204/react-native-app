import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from 'react-native';

export default class HopTrongNgay extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      refresh: false,
      page: 0,
      dataSource: [],
      date: new Date(),
    };
  }
  componentDidMount() {
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=&page=${this.state.page}&ngay=${this.state.date.getUTCDate()}&thang=${this.state.date.getMonth() + 1}&nam=${this.state.date.getFullYear()}&txts=`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  _onEndReached() { //chinh lai web service
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=&page=${this.state.page}&ngay=${this.state.date.getUTCDate()}&thang=${this.state.date.getMonth() + 1}&nam=${this.state.date.getFullYear()}&txts=`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.length !== 0) {
          this.setState({
            dataSource: this.state.dataSource.concat(responseJson),
            page: this.state.page + 1
          });
        } else {
          Alert.alert(
            'Thông báo',
            'Đã hết dữ liệu',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ]
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  loadNewData() {
    this.setState({
      refresh: true
    });
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=&page=0&ngay=${this.state.date.getUTCDate()}&thang=${this.state.date.getMonth() + 1}&nam=${this.state.date.getFullYear()}&txts=`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson,
          refresh: false
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={st.container}>
        <View style={st.thanh1}>
          <View style={{ width: 35, height: 35, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
              <Image style={st.icon_thanh} source={require('../resources/icons-menu-filled.png')} />
            </TouchableOpacity>
          </View>
          <View style={st.thanhphai}>
            <TouchableOpacity onPress={() => navigate('Login')}>
              <Image style={st.icon_thanh} source={require('../img/large-icon-user.png')} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Home')}>
              <Image style={st.icon_thanh} source={require('../img/home.png')} />
            </TouchableOpacity>
          </View>
        </View>

        <FlatList
          refreshing={this.state.refresh}
          onRefresh={() => { this.loadNewData(); }}
          onEndReached={this._onEndReached()}
          onEndReachedThreshold={0}
          data={this.state.dataSource}
          renderItem={({ item }) =>
            <TouchableOpacity style={st.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.key })}>
              <View style={st.bao}>
                <View style={st.bn}>
                  <View style={st.thu}>
                    <Text style={st.textw}>{item.Gio}</Text>
                    <Text style={st.textw}>Thứ {item.Thu}</Text>
                  </View>
                  <View style={st.ngay}>
                    <Text style={st.textb}>{item.Ngay}</Text>
                    <Text style={st.textb}>Tháng {item.Thang}</Text>
                  </View>
                  <View style={st.nam}>
                    <Text style={st.textw}>{item.Nam}</Text>
                  </View>
                </View>
                <View style={st.nd}>
                  <View style={st.bnd}>
                    <Text style={st.td}>{item.TenCH}</Text>
                  </View>
                  <View style={st.bct}>
                    <View style={st.ct}>
                      <Image style={st.icon} source={require('../img/company.png')} />
                      <Image style={st.icon} source={require('../img/marker.png')} />
                      <Image style={st.icon} source={require('../img/info.png')} />
                    </View>
                    <View style={st.ct}>
                      <Text style={st.txct}>{item.DonViToChuc}</Text>
                      <Text style={st.txct}>{item.DiaDiem}</Text>
                      <Text style={st.txct}>{item.TrangThai}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          }
        />

      </View>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
  },
  thanh1: {
    flexDirection: 'row',
    padding: 0,
    backgroundColor: 'red',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    justifyContent: 'flex-end',
  },
  thanhtrai: {

  },
  icon_thanh: {
    width: 35,
    height: 35,
    marginTop: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: 1
  },
  bct: {
    flexDirection: 'row',
  },
  nd: {
    marginLeft: 10,
    flex: 1
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  bn: {
    marginTop: 10
  },
  bnd: {
    minHeight: 30
  },
  thu: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    height: 40,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  icon: {
    width: 20,
    height: 20,
    marginTop: 10
  },
  ct: {
    marginTop: 0
  },
  txct: {
    marginLeft: 5,
    marginTop: 10,
    color: 'black'
  }
});
