import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions 
} from 'react-native';
import { Agenda } from 'react-native-calendars';

const { height } = Dimensions.get('window');

export default class MeetingSchedule extends Component {
  static navigationOptions = {
    drawerLabel: 'Lịch họp',
    // drawerIcon: () => (
    //   <Image
    //     source={require('./notif-icon.png')}
    //   />
    // ),
  };
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      refresh: false,
      dataSource: []
    };
  }

  render() {
    return (
      <Agenda
        onDayPress={this.onDayPress}
        items={this.state.items}         
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={Date.getDate}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        theme={{
          agendaDayTextColor: '#eb432d',
          agendaDayNumColor: '#eb432d',
          agendaTodayColor: '#eb432d',
          agendaKnobColor: '#eb432d'
        }}
      
      />
    );
  }
  
  loadItems(day) {
    setTimeout(() => {
      const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getngay?nam=${day.year}&thang=${day.month}`;
      console.log(url);
      fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Mảng trong fetch');
        console.log(responseJson);
        // return responseJson;
        this.setState({
          dataSource: responseJson
        });
      });
      
      // this.state.items =
      //   {
      //     '2017-11-09': [{ text: 'ngay 9' }],
      //     '2017-11-10': [{ text: 'ngay 10' }],
      //     '2017-11-11': [],
      //     '2017-11-12': [],
      //     '2017-11-13': [],
      //     '2017-11-14': [],
      //     '2017-11-15': [],
      //     '2017-11-16': [],
      //     '2017-11-17': [{ text: 'ngay 12 -1' }, { text: 'ngay 12 -2' }],
      //     '2017-12-18': [],
      //     '2017-12-19': [{ text: 'ngay 11 -tháng 12' }],
      //   };
      //console.log(`${'object sau khi get'}${this.state.items}`);

      
      setTimeout(() => {
        console.log('Mang sau khi get');
        console.log(this.state.dataSource); 
        this.state.items = this.state.dataSource;
        const newItems = {};
        Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
        this.setState({
          items: newItems
        });
        console.log(newItems);
        }, 2000);
    }, 1000);
    console.log(`Load Items for ${day.year}-${day.month}`);
  }
  
  renderItem(item) {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity style={styles.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.ID })}>
        <View style={[styles.item, { height: height / 10 }]}>
          <Text>{item.TenCH}</Text>
        </View>
      </TouchableOpacity >
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Không có lịch họp</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 40
  }
});
