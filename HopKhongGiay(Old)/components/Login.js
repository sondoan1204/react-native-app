import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  Image,
  Button,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';

import checkDangnhap from '../api/checkDangnhap';
import saveToken from '../api/saveToken';
import global from '../global';

const { width } = Dimensions.get('window');
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tk: '',
      mk: '',
    };
}
onSignIn() {
  const { tk, mk } = this.state;
  checkDangnhap(tk, mk)
    .then(res => {   
      if (res.thongbao === 'dung') {
        console.log('__________________');
        console.log(res.id);
        console.log(res.token);
        global.onSignIn(res);
        this.props.navigation.navigate('Home');
        saveToken(res.token);
      } else {
        Alert.alert(
          'Thông báo',
          'Sai thông tin đăng nhập',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
    })
    .then()
    .catch(err => { console.log('Lỗi'); console.log(err); });
}
  render() {
    return (
      <View style={styles.container}>

        <Button
          onPress={() => this.props.navigation.navigate('DrawerOpen')}
          title="Open Navigation"
        />
        <ScrollView >
        <View style={styles.container}>
          <Image
            source={require('../resources/logotayninh.png')}
            style={[styles.icon]}
          />
          <View style={styles.header}>
            <Text style={{ color: 'blue', fontSize: 10, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
            <Text style={{ color: 'blue', fontSize: 12, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
          </View>
        </View>
        <View style={{ justifyContent: 'space-between', flex: 2 }}>
          <View style={{ alignItems: 'center', marginTop: 25 }}>
            {/* <Text>Tên Đăng nhập</Text> */}
            <TextInput
              style={styles.inputStyle}
              placeholder="Nhập tên tài khoản"
              underlineColorAndroid="transparent"
              value={this.state.tk}
              onChangeText={text => this.setState({ tk: text })}
            //secureTextEntry
            />
            {/* <Text>Mật khẩu</Text> */}
            <TextInput
              style={styles.inputStyle}
              placeholder="Nhập mật khẩu"
              underlineColorAndroid="transparent"
              value={this.state.mk}
              onChangeText={text => this.setState({ mk: text })}
              secureTextEntry
            />
            <TouchableOpacity style={styles.bigButton} onPress={this.onSignIn.bind(this)}>
              <Text style={styles.buttonText}>ĐĂNG NHẬP</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text style={{ color: '#ea321a', marginTop: 15 }}>Quên mật khẩu?</Text>
            </TouchableOpacity>
          </View>
          <View style={{ alignItems: 'center', marginTop: 50 }}>
            <Text >Coppyright © Bản quyền thuộc Sở Thông tin</Text>
            <Text>và Truyền thông tỉnh Tây Ninh</Text>
          </View>
        </View>
        </ScrollView >

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
  icon: {
    // justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150,
  },
  header:
  {
    marginTop: 25,
    alignItems: 'center',
  },
  footer: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 20,
    paddingLeft: 30
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: '#ea321a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '400'
  }
});
