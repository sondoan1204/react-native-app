import React, { Component } from 'react';
import {
  StatusBar,
  Dimensions
} from 'react-native';

import Contact from './Contact';
import Home from './Home';
import DayMeeting from './DayMeeting';
import ListMeeting from './ListMeeting';
import Login from './Login';
import ChangePassword from './ChangePassword';
import MeetingSchedule from './MeetingSchedule';
import SlideMenu from './SlideMenu';
import DetailMeeting from './DetailMeeting';
import Search from './Search';

import { DrawerNavigator } from 'react-navigation';
import { LocaleConfig } from 'react-native-calendars';


LocaleConfig.locales.vn = {
  monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
  monthNamesShort: ['T1.', 'T2.', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12'],
  dayNames: ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
  dayNamesShort: ['CN', 'T2.', 'T3', 'T4', 'T5', 'T6', 'T7']
};

LocaleConfig.defaultLocale = 'vn';


StatusBar.setHidden(true);
const { width } = Dimensions.get('window');
const registerScreens = DrawerNavigator(
  {
    Home: {
      path: '/',
      screen: Home,
    },
    Login: {
      path: '/sent',
      screen: Login,
    },
    ChangePassword: {
      path: '/sent',
      screen: ChangePassword,
    },
    Contact: {
      path: '/sent',
      screen: Contact,
    },
    MeetingSchedule: {
      path: '/sent',
      screen: MeetingSchedule,
    },
    DetailMeeting: {
      path: '/sent',
      screen: DetailMeeting,
    },
    Search: {
      path: '/sent',
      screen: Search,
    },
    DayMeeting: {
      path: '/sent',
      screen: DayMeeting,
    },
  },
  {
    initialRouteName: 'Home',
    // drawerPosition: 'left',
    drawerWidth: (width / 3) * 2,
    contentComponent: props => <SlideMenu {...props} />,
  }
);

export default registerScreens;


// class HomeScreen extends React.Component {
//   static navigationOptions = {
//     title: 'Home'
//   };
//   render() {
//     return <Home />;
//   }
// }
// class LoginScreen extends React.Component {
//   static navigationOptions = {
//     title: 'Đăng Nhập'
//   };
//   render() {
//     return <Login />;
//   }
// }

// const SimpleApp = StackNavigator({
//   Home: { screen: HomeScreen },
//   Login: { screen: LoginScreen }
// });

// export default class App extends React.Component {
//   render() {

//     return (
//       <View style={styles.container}>

//         <SimpleApp />
//       </View>
//     );
//   }
// }


// export default class App extends React.Component {
//     render() {
//        return (
//            <View style={styles.container}>
//                <Text>Hop khong giay</Text>
//            </View>

//           // <Navigator
//           //   initialRoute={{ name: 'HOME' }}
//           //   renderScene={(route, navigator)=> {
//           //     switch (route.name) {
//           //               case 'HOME': return <Home navigator={navigator} />;
//           //               case 'CONTACT': return <Contact navigator={navigator} user={route.user} />;
//           //               case 'LOGIN': return <Login navigator={navigator} />;
//           //               case 'DAYMEETING': return <DayMeeting navigator={navigator} />;
//           //               case 'LISTMEETING': return <ListMeeting navigator={navigator} />;
//           //               default: return <MeetingSchedule navigator={navigator} />;
//           //           }
//           //   }}
//           // />
//        ); 
//     }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     //   justifyContent: 'center',
//     //   alignItems: 'center',
//     backgroundColor: '#EBB6AF',
//   }
// });
