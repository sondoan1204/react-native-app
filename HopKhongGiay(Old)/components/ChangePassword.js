import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  Image,
  Button,
  TextInput,
  TouchableOpacity,
  ScrollView
} from 'react-native';

const { width } = Dimensions.get('window');
export default class Login extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Đổi mật khẩu',
    // drawerIcon: () => (
    //   <Image
    //     source={require('./notif-icon.png')}
    //   />
    // ),
  };
  // constructor(props) {
  //   super(props);
  //   this.state = { text: 'Useless Placeholder' };
  // }
  render() {
    return (
      <View style={styles.container}>

        <Button
          onPress={() => this.props.navigation.navigate('DrawerOpen')}
          title="Open Navigation"
        />
        <ScrollView >
        <View style={styles.container}>
          <Image
            source={require('../resources/logotayninh.png')}
            style={[styles.icon]}
          />
          <View style={styles.header}>
            <Text style={{ color: 'blue', fontSize: 10, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
            <Text style={{ color: 'blue', fontSize: 12, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
          </View>
        </View>
        <View style={{ justifyContent: 'space-between', flex: 2 }}>
          <View style={{ alignItems: 'center', marginTop: 25 }}>
            {/* <Text>Tên Đăng nhập</Text> */}
            <TextInput
              style={styles.inputStyle}
              placeholder="Nhập tên tài khoản"
              underlineColorAndroid="transparent"
            //value={password}
            //onChangeText={text => this.setState({ password: text })}
            //secureTextEntry
            />

            <TextInput
              style={styles.inputStyle}
              placeholder="Mật khẩu cũ"
              secureTextEntry
              underlineColorAndroid="transparent"
            />
            <TextInput
              style={styles.inputStyle}
              placeholder="Số điện thoại"
              keyboardType='numeric'
              underlineColorAndroid="transparent"
            />
            {/* <Text>Mật khẩu</Text> */}
            <TextInput
              style={styles.inputStyle}
              placeholder="Nhập mật khẩu mới"
              underlineColorAndroid="transparent"
              //value={password}
              //onChangeText={text => this.setState({ password: text })}
              secureTextEntry
            />
            <TouchableOpacity style={styles.bigButton}>

              <Text style={styles.buttonText}>ĐỔI MẬT KHẨU</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Text style={{ color: '#ea321a', marginTop: 15 }}>Quên mật khẩu?</Text>
            </TouchableOpacity> */}
          </View>
          <View style={{ alignItems: 'center', marginTop: 20 }}>
            <Text >Coppyright © Bản quyền thuộc Sở Thông tin</Text>
            <Text>và Truyền thông tỉnh Tây Ninh</Text>
          </View>
        </View>
        </ScrollView >

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
  icon: {
    // justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 150,
  },
  header:
  {
    marginTop: 25,
    alignItems: 'center',
  },
  footer: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 20,
    paddingLeft: 30
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: '#ea321a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '400'
  }
});
