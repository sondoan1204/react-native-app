import { AsyncStorage } from 'react-native';

const getToken = async () => {
    try {
        const value = await AsyncStorage.getItem('@token');
        if (value !== null) {
            console.log('get token: value=');
            console.log(value);
            return value;
        }
        return '';
    } catch (error) {
        return '';
    }
};

export default getToken;
