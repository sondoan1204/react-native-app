import { AsyncStorage } from 'react-native';

const saveToken = async (token) => {
    try {
        await AsyncStorage.setItem('@token', token);
        console.log('save token thanh cong');
        console.log(token);
        return 'THANH_CONG';
    } catch (e) {
        return e;
    }
};

export default saveToken;
