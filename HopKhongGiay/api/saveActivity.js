import { AsyncStorage } from 'react-native';

const saveActivity = async (activity) => {
    try {
        await AsyncStorage.setItem('@activity', activity);
        console.log('save man hinh thanh cong');
        console.log(activity);
        return 'THANH_CONG';
    } catch (e) {
        return e;
    }
};

export default saveActivity;
