import { AsyncStorage } from 'react-native';

const getActivity = async () => {
    try {
        const value = await AsyncStorage.getItem('@activity');
        if (value !== null) {
            console.log('get man hinh: value=');
            console.log(value);
            return value.toString();
        }
        return 'Home';
    } catch (error) {
        return '';
    }
};

export default getActivity;
