import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  NetInfo,
  Alert
} from 'react-native';
import getToken from '../api/getToken';
import checkToken from '../api/checkToken';
import BackgroundTimer from 'react-native-background-timer';
import PushNotification from 'react-native-push-notification';
import global from '../global';
import { isSignedIn } from "../api/auth";

// Start a timer that runs continuous after X milliseconds
/*const intervalId = BackgroundTimer.setInterval(() => {
	// this will be executed every 200 ms
	// even when app is the the background
	console.log('tic');
}, 2000);

// Cancel the timer when you are done with it
//BackgroundTimer.clearInterval(intervalId);
// Start a timer that runs once after X milliseconds
const timeoutId = BackgroundTimer.setTimeout(() => {
	// this will be executed once after 10 seconds
	// even when app is the the background
  	console.log('tac');
}, 10000);*/

// Cancel the timeout if necessary
//BackgroundTimer.clearTimeout(timeoutId);
const { height } = Dimensions.get('window');
const { width } = Dimensions.get('window');


export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tb:0, 
      id: false ,
    };
    // global.onSignIn = this.onSignIn.bind(this);
  }
  static navigationOptions = {
    drawerLabel: 'Trang chủ',
  };
  showpush(msg) {
    PushNotification.configure({
      // (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
          console.log( 'NOTIFICATION:', notification );
      },
      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,
      requestPermissions: true,
  });
    PushNotification.localNotificationSchedule({
      title: "HKG Thông Báo", // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
      message: msg, // (required)
      playSound: true, // (optional) default: true
      soundName: 'default',
      repeatType: 'time',
      date: new Date(Date.now()) // in 60 secs
    });
  };
  checktb() {
    getToken()
    .then(token => {
      if(token.length > 0){
        var paramsString = "token="+ token;
        url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/hopmoi`;
        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
        })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.length !== 0) {
            this.setState({
              tb: responseJson.length
            });
            if(responseJson.length == 1)
              this.showpush("Bạn có 1 cuộc họp mới: " +responseJson[0].Ten);
            else {
              this.showpush("Bạn có "+ responseJson.length +" cuộc họp mới");
            }
          }          
        })
        .catch((err) => {
          console.log(err);
        });
      }       
    })
  }  

  // componentWillMount(){
  //   isSignedIn()
  //   .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
  //   .catch(err => alert("An error occurred"));
  //   console.log('_____________ ++++++++++++++++++++++++______________');
  //   console.log(this.state.checkedSignIn);
  //   console.log(this.state.signedIn);
  // }
  componentDidMount() {
    console.log('_____________ componentDidMount');
    const intervalId = BackgroundTimer.setInterval(() => {
      // this will be executed every 200 ms
      // even when app is the the background
      this.checktb();
      
    }, 30000);
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
    });
    function handleFirstConnectivityChange(isConnected) {
      if (isConnected == false) {
        Alert.alert(
          'Thông báo',
          'Vui lòng bật kết nối Internet',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
      console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
      NetInfo.isConnected.removeEventListener(
        'change',
        handleFirstConnectivityChange
      );
    }
    NetInfo.isConnected.addEventListener(
      'change',
      handleFirstConnectivityChange
    );
    setTimeout(() => {
      getToken()
        .then(token => checkToken(token))
        .then(res => {
          if (res.thongbao === 'dung') {
            global.onSignIn(res);
            this.setState(previousState => {
              return { id: res };
            });
            // this.setState({ id: res });
          }else{
            this.setState(previousState => {
              return { id: false };
            });
          }
        })
        .catch(err => console.log('LOI CHECK lỗi ____________', err));
    }, 100);
  }
  

componentWillReceiveProps(){
  getToken()
  .then(token => checkToken(token))
  .then(res => {
    if (res.thongbao === 'dung') {
      // global.onSignIn(res);
      this.setState(previousState => {
        return { id: res };
      });
      // this.setState({ id: res });
    }else{
      this.setState(previousState => {
        return { id: false };
      });
    }
  })
  .catch(err => console.log('LOI CHECK lỗi ____________', err));
}
  // shouldComponentUpdate() {
  //   getToken()
  //   .then(token => checkToken(token))
  //   .then(res => {
  //     if (res.thongbao === 'dung') {
  //       global.onSignIn(res);
  //       this.setState({ id: res });
  //       return true;
  //     }else{
  //       this.setState({ id: false });
  //       return true;        
  //     }
  //   })
  //   .catch(err => console.log('LOI CHECK lỗi ____________', err));
  // }
  // componentWillUpdate() {
  //   // Hàm này thực hiện dựa vào kết quả của hàm trên (shouldComponentUpdate)
  //   // Nếu hàm trên trả về false, thì React sẽ không gọi hàm này
  // }  
  // componentWillUpdate() {
  //   console.log('componentWillUpdate');
  // }

  // componentDidUpdate() {
  //   console.log('componentDidUpdate');
  // }

  // onSignIn(id) {
  //   this.setState({ id });
  //   console.log('++In home menu +++++++++');
  //   console.log(id);
  // }  
  render() {
    const { id } = this.state;
    const { navigate } = this.props.navigation;
    const viewbtnLogin = (
      <View style={st.thanhphai}>
      <TouchableOpacity onPress={() => navigate('Login')}>
        <Image style={st.icon_thanh} source={require('../img/large-icon-user.png')} />
      </TouchableOpacity>
      </View>
    );
    const viewbtnUser = (
      <View style={{ marginTop: 15, flex: 1 }}>
      <Text style={st.text_thanh}>{id ? id.Ten : ''}</Text>
      </View>
    );
    
    const mainJSX = this.state.id ? viewbtnUser : viewbtnLogin;
    return (
      //   <View style={styles.container}>
      //     <View style={{ height: height / 10, backgroundColor: '#fff' }} >
      //       <Button
      //         onPress={() => this.props.navigation.navigate('DrawerOpen')}
      //         title="Open Navigation"
      //       />
      // </View>
      //       <Text>DayMeeting</Text>
      //      </View>
      <View style={{ flex: 1, justifyContent: 'space-between' }}>
        {/* <Header /> */}

        <View >
          <View style={st.thanh1}>
            <View style={{ width: 30, height: 30, justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                <Image style={st.icon_menu} source={require('../resources/icons-menu-filled.png')} />
              </TouchableOpacity>
            </View>
            {/* <View style={st.thanhphai}>
              <TouchableOpacity onPress={() => navigate('Login')}>
                <Image style={st.icon_thanh} source={require('../img/large-icon-user.png')} />
              </TouchableOpacity>
            </View> */}
            {mainJSX}
          </View>
          <View style={st.thanh2}>
            <Image style={st.icon_logo} source={require('../img/logotayninh.png')} />
          </View>
          <View style={st.thanh2}>
            <Text style={{ color: 'blue', fontSize: 11.5, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
          </View>
          <View style={st.thanh2}>
            <Text style={{ color: 'blue', fontSize: 11.5, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
          </View>
        </View>

        <View >
          <View style={st.thanh2}>
            <TouchableOpacity onPress={() => navigate('Search')}>
              <Image style={st.icon} source={require('../img/ctl.png')} />
              <Text style={st.textDS}>Danh Sách</Text>
              <Text style={st.textDS2}>Cuộc Họp</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('MeetingSchedule',{ idtentv: this.state.id ? this.state.id.id: '' })}>
              <Image style={st.icon} source={require('../img/calendar-icon.png')} />
              <Text style={st.textDS2}>Lịch Họp</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('DayMeeting')}>
              <Image style={st.icon} source={require('../img/checklist-on-clipboard.png')} />
              <Text style={st.textDS}>Cuộc Họp</Text>
              <Text style={st.textDS}>Trong Ngày</Text>
            </TouchableOpacity>
          </View>
          <View style={st.thanh2} />
          <View style={st.thanh2}>
            <TouchableOpacity onPress={() => navigate('NewMeeting')}>
              <Image style={st.icon} source={require('../resources/user.png')} />              
              <Text style={st.textDS}>Cuộc họp mới</Text>              
              <Text style={st.tbnb}>{this.state.tb}</Text>           
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('ThongBao')}> 
              <Image style={st.icon} source={require('../resources/thongbao.png')} />
              <Text style={st.textDS}>Thông báo</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Contact')}>
              <Image style={st.icon} source={require('../resources/contact.png')} />
              <Text style={st.textDS2}>Liên Hệ</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View >
          <View style={st.footer}>
            <Text >Copyright © Bản quyền thuộc </Text>
            <Text>Sở Thông Tin Và Truyền Thông Tỉnh Tây Ninh</Text>
          </View>
        </View>
      </View>
    );
  }
}

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//     //   justifyContent: 'center',
//     //   alignItems: 'center',
//       backgroundColor: '#EB432D',
//     }
//   });
const st = StyleSheet.create({
  thanh1: {
    flexDirection: 'row',
    padding: 0,
    backgroundColor: 'red',
    // height: 55,
    justifyContent: 'space-between',
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    justifyContent: 'flex-end',
  },
  thanhtrai: {
    // flexDirection: 'row',
    // padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    // justifyContent: 'flex-end',
  },
  thanh2: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 0,
  },
  icon: {
    width: width / 15 * 3,
    height: width / 15 * 3,
    marginTop: 15,
    marginRight: 35,
    marginLeft: 25,
    marginBottom: 1
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	  marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  text_thanh: {
    color: '#FFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
    justifyContent: 'center',
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
  	marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
  },
  icon_logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
    height: width / 15 * 5,
    width: width / 15 * 5,
  },
  textDS: {
    marginTop: 0,
    marginLeft: 0,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:'center'
  },
  tbnb: {
    width: 20,
    height:20,
    marginTop: -85,
    marginLeft:75,
    color: 'white',
    backgroundColor: 'red',
    fontWeight: 'bold',
    fontSize: 11,
    borderRadius: 10,
    overflow:'hidden',
    textAlign:'center',
    padding: 3
  },
  textDS2: {
    marginTop: 0,
    marginLeft: 0,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 10,
    justifyContent: 'center',
    textAlign:'center'
  },
  footer: {
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
