import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity
} from 'react-native';
import getToken from '../api/getToken';

export default class HopTrongNgay extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
	    idcn: '',
	    crl: true
    };
  }
  componentDidMount() {
	var url = ``;
	getToken()
	.then(token => {
		if(token.length > 0){
			var paramsString = "token="+ token;
			url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/dshopmoi`;
      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: paramsString
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.length !== 0) {
          this.setState({
            dataSource: responseJson
          });
        }
        else {
          Alert.alert(
            'Thông báo',
            'Không có cuộc họp mới',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ]
          );
        }
      })
      .catch((err) => {
        console.log(err);
      });
		} else {
			Alert.alert(
        'Thông báo',
        'Bạn cần đăng nhập trước để xem',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
		}
		
	})
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
		<View style={{flex:1}}>
			<View style={st.thanh1}>
				<View style={{ width: 25, height: 25, justifyContent: 'center'}}>
				  <TouchableOpacity onPress={() => navigate('Home')}>
					<Image style={st.icon_menu} source={require('../resources/back.png')} />
				  </TouchableOpacity>					
				</View>
				<View style={{marginTop:15,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold'}}>Cuộc họp mới</Text>
				</View>
			</View>
			<FlatList
			  data={this.state.dataSource}
			  renderItem={({ item }) =>
				<TouchableOpacity style={st.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.key,screen: 'NewMeeting' })}>
				  <View style={st.bao}>
					<View style={st.bn}>
					  <View style={st.thu}>
						<Text style={st.textw}>{item.Gio}</Text>
						<Text style={st.textw}>Thứ {item.Thu}</Text>
					  </View>
					  <View style={st.ngay}>
						<Text style={st.textb}>{item.Ngay}</Text>
						<Text style={st.textb}>Tháng {item.Thang}</Text>
					  </View>
					  <View style={st.nam}>
						<Text style={st.textw}>{item.Nam}</Text>
					  </View>
					</View>
					<View style={st.nd}>
					  <View style={st.bnd}>
						<Text style={item.TrangThai =='Chờ họp' ? st.td : item.TrangThai =='Tạm hoãn' ? st.tdd : st.tdb}>{item.TenCH}</Text>
					  </View>
					  <View style={st.bct}>
						<View style={st.ct}>
						  <Image style={st.icon} source={require('../img/company.png')} />
						  <Image style={st.icon} source={require('../img/marker.png')} />
						  <Image style={st.icon} source={require('../img/info.png')} />
						</View>
						<View style={st.ct}>
						  <Text style={st.txct}>{item.DonViToChuc}</Text>
						  <Text style={st.txct}>{item.DiaDiem}</Text>
						  <Text style={st.txct}>{item.TrangThai}</Text>
						</View>
					  </View>
					</View>
				  </View>
				</TouchableOpacity>
			  }
			/>

		</View>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
  },
  thanh1: {
    paddingBottom: 15,
    backgroundColor: 'red',
		justifyContent: 'space-between',
		flexDirection: 'row',
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    justifyContent: 'flex-end',
  },
  thanhtrai: {

  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
  },
  nd: {
    marginLeft: 10,
    flex: 1
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  tdb: {
	fontWeight: 'bold',
	color: 'blue'
  },
  tdd: {
	fontWeight: 'bold',
	color: 'red'
  },
  bn: {
    marginTop: 10
  },
  bnd: {
    minHeight: 30
  },
  thu: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow:'hidden',
    height: 40,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  icon: {
    width: 20,
    height: 20,
    marginTop: 10
  },
  ct: {
    marginTop: 0
  },
  txct: {
		marginLeft: 5,
		marginTop: 15,
		color: 'black',
		fontSize:12,
	},
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
		marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
  },
});
