import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  Image,
  Button,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';

import checkDangnhap from '../api/checkDangnhap';
import saveToken from '../api/saveToken';
import global from '../global';
// import { regonSignIn } from "../api/auth";

const { width } = Dimensions.get('window');
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tk: '',
      mk: '',
    };
}
onSignIn() {
  const { tk, mk } = this.state;
  checkDangnhap(tk, mk)
    .then(res => {   
      if (res.thongbao === 'dung') {
        console.log('__________________');
        console.log(res.id);
        console.log(res.token);
        global.onSignIn(res);
        // regonSignIn();
        this.props.navigation.navigate('Home');
        saveToken(res.token);
      } else {
        Alert.alert(
          'Thông báo',
          'Sai thông tin đăng nhập',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
    })
    .then()
    .catch(err => { console.log('Lỗi'); console.log(err); });
}
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1}}>
      <View style={styles.thanh1}>
          <View style={{ width: 25, height: 25, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => navigate('Home')}>
              <Image style={styles.icon_menu} source={require('../resources/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 15, flex: 1 }}>
            {/* <Text style={{ textAlign: 'center', fontSize: 16, color: 'white', fontWeight: 'bold' }}>Cuộc họp trong ngày</Text> */}
          </View>
        </View>
        <View style={styles.container} >
			<View>
			  <View style={{marginTop:20, alignItems:'center'}}>
			  <Image
				source={require('../resources/logotayninh.png')}
				style={[styles.icon]}
			  />
			  </View>
			  <View style={styles.header}>
				<Text style={{ color: 'blue', fontSize: 11, fontWeight: 'bold' }}>ỦY BAN NHÂN DÂN TỈNH TÂY NINH </Text>
				<Text style={{ color: 'blue', fontSize: 13, fontWeight: 'bold' }}>HỆ THỐNG THÔNG TIN ĐIỀU HÀNH - HỌP KHÔNG GIẤY </Text>
			  </View>
			</View>
			<View style={{ justifyContent: 'space-between', flex: 2 }}>
			  <View style={{ alignItems: 'center', marginTop: 25 }}>
				{/* <Text>Tên Đăng nhập</Text> */}
				<TextInput
				  style={styles.inputStyle}
				  placeholder="Nhập tên tài khoản"
				  underlineColorAndroid="transparent"
				  value={this.state.tk}
				  onChangeText={text => this.setState({ tk: text })}
				//secureTextEntry
				/>
				{/* <Text>Mật khẩu</Text> */}
				<TextInput
				  style={styles.inputStyle}
				  placeholder="Nhập mật khẩu"
				  underlineColorAndroid="transparent"
				  value={this.state.mk}
				  onChangeText={text => this.setState({ mk: text })}
				  secureTextEntry
				/>
				<TouchableOpacity style={styles.bigButton} onPress={this.onSignIn.bind(this)}>
				  <Text style={styles.buttonText}>ĐĂNG NHẬP</Text>
				</TouchableOpacity>
			  </View>
			  <View style={styles.footer}>
				<Text >Coppyright © Bản quyền thuộc Sở Thông Tin</Text>
				<Text>Và Truyền Thông Tỉnh Tây Ninh</Text>
			  </View>
			</View>
        </View >

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
  icon: {
    // justifyContent: 'center',
    alignItems: 'center',
    width: 140,
    height: 140,
	paddingTop: 15
  },
  header:
  {
    marginTop: 25,
    alignItems: 'center',
  },
  footer: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 20,
    paddingLeft: 30
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: '#ea321a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '400'
  },
  thanh1: {
    paddingBottom: 15,
    backgroundColor: 'red',
    justifyContent: 'space-between',
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
  },
  footer: {
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
