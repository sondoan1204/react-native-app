import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
import { Agenda } from 'react-native-calendars';
import getToken from '../api/getToken';

const { height } = Dimensions.get('window');

export default class MeetingSchedule extends Component {
  static navigationOptions = {
    drawerLabel: 'Lịch họp',
    // drawerIcon: () => (
    //   <Image
    //     source={require('./notif-icon.png')}
    //   />
    // ),
  };
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      items: {},
      date: new Date(),
      refresh: false,
      dataSource: [],
      idcn: params.idtentv
    };
  }
  // componentDidMount() {
  //   getToken()
  //     .then(token => {
  //       if (token.length > 0) {
  //         var paramsString = "token=" + token;
  //         fetch("http://hkg.tayninh.gov.vn/services/WebService.asmx/checktoken", {
  //           method: 'POST',
  //           headers: {
  //             'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  //           },
  //           body: paramsString
  //         })
  //           .then((response) => response.json())
  //           .then((responseJson) => {
  //             console.log(responseJson);
  //             if (responseJson.thongbao == 'dung') {
  //               this.setState({
  //                 idcn: responseJson.id
  //               })
  //             }
  //           })
  //           .catch(err => console.log('LOI ', err));
  //       }
  //     })
  // }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.thanh1}>
          <View style={{ width: 25, height: 25, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => navigate('Home')}>
              <Image style={styles.icon_menu} source={require('../resources/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 15, flex: 1 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: 'white', fontWeight: 'bold' }}>Lịch họp</Text>
          </View>
        </View>
        <Agenda
          dayLoading={true}
          items={this.state.items}
          loadItemsForMonth={this.loadItems.bind(this)}
          selected={Date.getDate}
          // minDate={'2017-11-01'}
          // maxDate={'2017-11-30'}
          renderItem={this.renderItem.bind(this)}
          renderEmptyDate={this.renderEmptyDate.bind(this)}
          rowHasChanged={this.rowHasChanged.bind(this)}
          theme={{
            agendaDayTextColor: '#eb432d',
            agendaDayNumColor: '#eb432d',
            agendaTodayColor: '#eb432d',
            agendaKnobColor: '#eb432d'
          }}
          onDayPress={this.onDayPress}
        />
      </View>
    );
  }

  loadItems(day) {
    setTimeout(() => {
      const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getngay?nam=${day.year}&thang=${day.month}&idtv=${this.state.idcn}`;
      console.log(url);
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log('Mảng trong fetch');
          // console.log(responseJson);
          // return responseJson;
          this.setState({
            items: responseJson
          });
        });
      setTimeout(() => {
        // console.log('Mang sau khi get');
        // console.log(this.state.dataSource);
        //this.state.items = this.state.dataSource;
        const newItems = {};
        Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
        this.setState({
          items: newItems
        });
        // console.log(newItems);
      }, 2000);
    }, 1000);
    console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity style={styles.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.ID, screen: 'MeetingSchedule' })}>
        <View style={[styles.item,]}>
          <Text onLayout={this.handleTextLayout}>{item.TenCH}</Text>
        </View>
      </TouchableOpacity >
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Không có lịch họp</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  // timeToString(time) {
  //   const date = new Date(time);
  //   return date.toISOString().split('T')[0];
  // }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 40
  },
  thanh1: {
    paddingBottom: 15,
    backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
  },
});
