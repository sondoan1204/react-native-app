import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Text,
  FlatList,
  Alert,
} from 'react-native';
import getToken from '../api/getToken';

const { height } = Dimensions.get('window');

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      txtSearch: '',
      refresh: false,
      page: 0,
      dataSource: [],
      idcn: '',
      crl: true
    };
  }
  componentDidMount() {
    var url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=&page=0&ngay=0&thang=0&nam=0&loai=1&txts=`;
    getToken()
      .then(token => {
        if (token.length > 0) {
          var paramsString = "token=" + token;
          fetch("http://hkg.tayninh.gov.vn/services/WebService.asmx/checktoken", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: paramsString
          })
            .then((response) => response.json())
            .then((responseJson) => {
              console.log(responseJson);
              if (responseJson.thongbao == 'dung') {
                this.setState({
                  idcn: responseJson.id
                })
                url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${responseJson.id}&page=0&ngay=0&thang=0&nam=0&loai=1&txts=`;
                fetch(url)
                  .then((response) => response.json())
                  .then((responseJson) => {
                    if (responseJson.length !== 0) {
                      this.setState({
                        dataSource: responseJson
                      });
                    }
                    else {
                      Alert.alert(
                        'Thông báo',
                        'Không có cuộc họp nào',
                        [
                          { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ]
                      );
                    }
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              }
            })
            .catch(err => console.log('LOI ', err));
        } else {
          fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
              if (responseJson.length !== 0) {
                this.setState({
                  dataSource: responseJson
                });
              }
              else {
                Alert.alert(
                  'Thông báo',
                  'Không có cuộc họp nào',
                  [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                  ]
                );
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }

      })
  }
  onSearch() {
    const { txtSearch } = this.state;
    //this.setState({ txtSearch: '' });
    console.log('OnSearch');
    console.log(this.state.txtSearch);
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${this.state.idcn}&page=0&ngay=0&thang=0&nam=0&loai=1&txts=${txtSearch}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataSource: responseJson
        });
        console.log('Danh sach');
        console.log(this.state.dataSource);
      })
      .catch((err) => {
        console.log('Loi');
        console.log(err);
      });
  }
  _onEndReached() {
    if (this.state.crl == true) {
      var url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=&page=${this.state.page}&ngay=0&thang=0&nam=0&loai=1&txts=${this.state.txtSearch}`;
      getToken()
        .then(token => {
          if (token.length > 0) {
            var paramsString = "token=" + token;
            fetch("http://hkg.tayninh.gov.vn/services/WebService.asmx/checktoken", {
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
              },
              body: paramsString
            })
              .then((response) => response.json())
              .then((responseJson) => {
                console.log(responseJson);
                if (responseJson.thongbao == 'dung') {
                  this.setState({
                    idcn: responseJson.id
                  })
                  url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${responseJson.id}&page=${this.state.page}&ngay=0&thang=0&nam=0&loai=1&txts=${this.state.txtSearch}`;
                  fetch(url)
                    .then((response) => response.json())
                    .then((responseJson) => {
                      if (this.state.page > 0 && responseJson.length > 0) {
                        this.setState({
                          dataSource: this.state.dataSource.concat(responseJson),
                          page: this.state.page + 1
                        });
                      } else {
                        this.setState({
                          crl: false
                        });
                      }
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                }
              })
              .catch(err => console.log('LOI ', err));

          } else {
            fetch(url)
              .then((response) => response.json())
              .then((responseJson) => {
                if (responseJson.length !== 0) {
                  this.setState({
                    dataSource: this.state.dataSource.concat(responseJson),
                    page: this.state.page + 1
                  });
                } else {
                  this.setState({
                    crl: false
                  });
                }
              })
              .catch((err) => {
                console.log(err);
              });
          }

        })
    }
  }
  loadNewData() {
    this.setState({
      refresh: true
    });
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${this.state.idcn}&page=0&ngay=0&thang=0&nam=0&loai=1&txts=${this.state.txtSearch}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.length !== 0) {
          this.setState({
            dataSource: responseJson,
            refresh: false
          });
        }
        else {
          Alert.alert(
            'Thông báo',
            'Không có cuộc họp nào',
            [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ]
          );
          this.setState({
            refresh: false
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.row1}>
            {/* <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                            <Image style={styles.iconStyle} source={require('../resources/icons-menu-filled.png')} />
                        </TouchableOpacity> */}
            <View style={{ width: 25, height: 25, justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => navigate('Home')}>
                <Image style={styles.icon_menu} source={require('../resources/back.png')} />
              </TouchableOpacity>
            </View>
            {/* <TouchableOpacity onPress={this.props.onOpen}>
                        <Image source={styles.icMenu} style={styles.iconStyle} />
                    </TouchableOpacity> */}
            <Text style={styles.titleStyle}>Tìm kiếm</Text>
            <Image source={styles.icLogo} style={styles.iconStyle} />
          </View>
          <TextInput
            style={styles.textInput}
            placeholder="Nhập tên cuộc họp"
            underlineColorAndroid="transparent"
            value={this.state.txtSearch}
            onChangeText={text => this.setState({ txtSearch: text })}
            onSubmitEditing={this.onSearch.bind(this)}
          />
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            refreshing={this.state.refresh}
            onRefresh={() => { this.loadNewData(); }}
            onEndReached={this._onEndReached()}
            onEndReachedThreshold={0}
            data={this.state.dataSource}
            // extraData={this.state.dataSource}
            renderItem={({ item }) =>
              <TouchableOpacity style={styles.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.key, screen: 'Search' })}>
                <View style={styles.bao}>
                  <View style={styles.bn}>
                    <View style={styles.thu}>
                      <Text style={styles.textw}>{item.Gio}</Text>
                      <Text style={styles.textw}>Thứ {item.Thu}</Text>
                    </View>
                    <View style={styles.ngay}>
                      <Text style={styles.textb}>{item.Ngay}</Text>
                      <Text style={styles.textb}>Tháng {item.Thang}</Text>
                    </View>
                    <View style={styles.nam}>
                      <Text style={styles.textw}>{item.Nam}</Text>
                    </View>
                  </View>
                  <View style={styles.nd}>
                    <View style={styles.bnd}>
                      <Text style={item.TrangThai == 'Chờ họp' ? styles.td : item.TrangThai == 'Tạm hoãn' ? styles.tdd : styles.tdb}>{item.TenCH}</Text>
                    </View>
                    <View style={styles.bct}>
                      <View style={styles.ct}>
                        <Image style={styles.icon} source={require('../img/company.png')} />
                        <Image style={styles.icon} source={require('../img/marker.png')} />
                        <Image style={styles.icon} source={require('../img/info.png')} />
                      </View>
                      <View style={styles.ct}>
                        <Text style={styles.txct}>{item.DonViToChuc}</Text>
                        <Text style={styles.txct}>{item.DiaDiem}</Text>
                        <Text style={styles.txct}>{item.TrangThai}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            }
          />
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    height: height / 8,
    backgroundColor: 'red',
    padding: 10,
    justifyContent: 'space-around'
  },
  row1: { flexDirection: 'row', justifyContent: 'space-between' },
  textInput: {
    height: height / 23,
    backgroundColor: '#FFF',
    paddingLeft: 10,
    paddingVertical: 0
  },
  titleStyle: { color: '#FFF', fontFamily: 'Avenir', fontSize: 20 },
  iconStyle: { width: 25, height: 25 },
  bao: {
    flexDirection: 'row',
    padding: 10,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
  },
  nd: {
    marginLeft: 10,
    flex: 1
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  tdb: {
    fontWeight: 'bold',
    color: 'blue'
  },
  tdd: {
    fontWeight: 'bold',
    color: 'red'
  },
  bn: {
    marginTop: 10
  },
  bnd: {
    minHeight: 30
  },
  thu: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow: 'hidden',
    height: 40,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    overflow: 'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  icon: {
    width: 20,
    height: 20,
    marginTop: 10
  },
  ct: {
    marginTop: 0
  },
  txct: {
    marginLeft: 5,
    marginTop: 15,
    color: 'black',
    fontSize: 12,
  }
});


AppRegistry.registerComponent('search', () => Search);
