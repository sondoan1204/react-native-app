import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
  Linking
} from 'react-native';
import getToken from '../api/getToken';

const newLocal = true;

export default class DetailMeeting extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    //console.log(params);
    this.state = {
      key: params.idcuochop,
      screen: params.screen,
      dataHop: [],
      dataDV: [],
      dataFile: [],
      viewtp: true,
      active: 'ThanhPhan',
    };
  }

  componentDidMount() {
    const url = `http://hkg.tayninh.gov.vn/services/WebService.asmx/getcthop?idh=${this.state.key}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataHop: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
    const urltp = `http://hkg.tayninh.gov.vn/services/WebService.asmx/gettphop?idh=${this.state.key}`;
    fetch(urltp)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataDV: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
    const urltl = `http://hkg.tayninh.gov.vn/services/WebService.asmx/gettlhop?idh=${this.state.key}`;
    fetch(urltl)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataFile: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
  }
  _onPressTP() {
    this.setState({
      viewtp: true,
      active: 'ThanhPhan',
    });
    console.log(this.state.active);
  }
  _onPressTL() {
    this.setState({
      viewtp: false,
      active: 'TaiLieu',
    });
    console.log(this.state.active);
  }
  taifile(key) {
    getToken()
    .then(token => {
      if(token.length > 0){
      var paramsString = "id="+ key +"&token="+ token;
      fetch("http://hkg.tayninh.gov.vn/services/WebService.asmx/CodeFile", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.thongbao == 'dung'){
          var link = 'http://hkg.tayninh.gov.vn/services/WebService.asmx/download?code='+responseJson.code;
          Linking.canOpenURL(link).then(supported => {
            if (supported) {
              Linking.openURL(link);
            } else {
              console.log("Don't know how to open URI: " + link);
            }
          });
        }        
      })
      .catch((err) => {
        console.log('Loi');
      });
    } else {
      Alert.alert(
        'Thông báo',
        'Bạn phải đăng nhập để lấy tài liệu',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }
    })
    .catch(err => console.log('LOI CHECK LOGIN', err));
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1}}>
        <View style={st.thanh1}>
          <View style={{ width: 25, height: 25, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => navigate(this.state.screen)}>
            <Image style={st.icon_menu} source={require('../resources/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{marginTop:15,flex:1}}>
            <Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold'}}>Chi tiết cuộc họp</Text>
          </View>
        </View>
        <View style={st.container}>
          <View style={st.ngoai}>
            <View>
              <FlatList
                data={this.state.dataHop}
                renderItem={({ item }) =>
                  <View style={st.bao}>
                    <View style={st.bt}>
                      <View style={st.bn}>
                        <View style={st.thu}>
                          <Text style={st.textw}>Thứ {item.Thu}</Text>
                        </View>
                        <View style={st.ngay}>
                          <Text style={st.textb}>{item.Ngay}</Text>
                          <Text style={st.textb}>Tháng {item.Thang}</Text>
                        </View>
                        <View style={st.nam}>
                          <Text style={st.textw}>{item.Nam}</Text>
                        </View>
                      </View>
                      <View style={st.nd}>
                        <View style={st.bnd}>
                          <Text style={st.td}>{item.TenCH}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={st.bct}>
                      <View style={st.ct}>
                        <View style={st.ct2}>
                          <View style={st.ct3}>
                            <Text style={st.txct}>Giờ họp: {item.Gio}</Text>
                            <Text style={st.txct}>Loại hình họp: {item.TenLHH}</Text>
                          </View>
                          <View style={st.ct3}>
                            <Text style={st.txct}>Lĩnh vực họp: {item.TenLV}</Text>
                            <Text style={st.txct}>Hình thức họp: {item.TenHT}</Text>
                          </View>
                        </View>
                        <View>
                          <Text style={st.txct}>Địa điểm họp: {item.DDHop}</Text>
                          <Text style={st.txct}>Trạng thái: {item.TrangThai}</Text>
                          <Text style={st.txnd}>Nội dung cuộc họp</Text>
                          <Text style={st.txct}>{item.NoiDung}</Text>
                        </View>
                        <View style={st.ct4}>
                          {/* <TouchableHighlight underlayColor='transparent' style={st.tou} onPress={() => { this.setState({ active: 'ThanhPhan' }); }}>
                            <Text style={[st.txna, (this.state.active === 'TaiLieu' ? st.activetxna : {})]}>Thành phần tham dự</Text>
                          </TouchableHighlight >
                          <TouchableHighlight underlayColor='transparent' style={st.tou} onPress={() => { this.setState({ active: 'TaiLieu' }); }}>
                            <Text style={[st.txna, (this.state.active === 'ThanhPhan' ? st.activetxna : {})]}>Tài liệu liên quan</Text>
                          </TouchableHighlight > */}
                          <TouchableOpacity style={st.tou} onPress={this._onPressTP.bind(this)}>
                            <Text style={[st.txna, (this.state.viewtp === true ? st.activetxna : {})]}>Thành phần tham dự</Text>
                          </TouchableOpacity >
                          <TouchableOpacity style={st.tou} onPress={this._onPressTL.bind(this)}>
                            <Text style={[st.txna, (this.state.viewtp === false ? st.activetxna : {})]}>Tài liệu liên quan</Text>
                          </TouchableOpacity >
                        </View>
                      </View>
                    </View>
                  </View>
                }
              />
            </View>
            <ScrollView style={this.state.viewtp ? st.it2 : {}}>
              <FlatList
                data={this.state.dataFile}
                renderItem={({ item }) =>
                  <View style={st.bao}>
                    <TouchableOpacity onPress={this.taifile.bind(this,item.ID)}>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image style={st.icon} source={require('../resources/fileformatpdf.png')} />
                        <Text style={st.txct}>{item.FileTL}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                }
              />
            </ScrollView>
            <ScrollView style={this.state.viewtp ? {} : st.it2}>
              <FlatList
                data={this.state.dataDV}
                renderItem={({ item }) =>
                  <View style={st.bao}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Image style={st.icon} source={require('../resources/organizationwf.png')} />
                      <Text style={st.txct}>{item.Ten}</Text>
                    </View>
                  </View>
                }
              />
            </ScrollView>

          </View>
        </View>
      </View>
    );
  }
}

const st = StyleSheet.create({
  container: {
    flex: 1,
    //   justifyContent: 'center',
    //   alignItems: 'center',
    //backgroundColor: '#EB432D',
  },
  icon: {
    width: 25,
    height: 25
  },
  bao: {
    flexDirection: 'column',
    padding: 5,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
    flex: 1
  },
  bt: {
    flexDirection: 'row',
  },
  nd: {
    marginLeft: 10,
    flex: 1
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  bn: {
    marginTop: 5
  },
  thu: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  ct: {
    marginTop: 0,
    flexDirection: 'column',
    flex: 1
  },
  ct2: {
    flexDirection: 'row',

  },
  ct3: {
    flex: 2
  },
  ct4: {
    flex: 1,
    flexDirection: 'row',
  },
  txct: {
    fontFamily: 'Avenir',
    marginLeft: 5,
    marginTop: 5,
    color: 'black',
    fontSize:11
  },
  txnd: {
    marginTop: 5,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    fontWeight: 'bold',
    color: 'black',
    backgroundColor: '#80ceff'
  },
  ngoai: {
    flexDirection: 'column',
    flex: 1
  },
  txna: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#fb1c1c',
    marginLeft: 1,
    marginRight: 1,
  },
  activetxna: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ff9688',
    marginLeft: 1,
    marginRight: 1,
  },
  txnn: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ec6060',
    marginLeft: 1,
    marginRight: 1,
  },
  tou: {
    flex: 1,
    marginTop: 5,
  },
  it2: {
    height: 0,
    width: 0
  },
  thanh1: {
		paddingBottom: 15,
		backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginTop: 20,
		marginBottom: 0,
		marginRight: 10,
		marginLeft: 5,
	},
});
