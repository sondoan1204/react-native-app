import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions,TouchableOpacity } from 'react-native';
// import MapView from 'react-native-maps';

export default class Contact extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Liên hệ',
    // drawerIcon: () => (
    //   <Image
    //     source={require('./notif-icon.png')}
    //   />
    // ),
  };
  render() {
    const { navigate } = this.props.navigation;
    const {
      mapContainer, wrapper, infoContainer,
      rowInfoContainer, imageStyle, infoText, lableText
  } = styles;
    return (
      <View style={{ flex: 1 }}>
<View style={styles.thanh1}>
          <View style={{ width: 25, height: 25, justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => navigate('Home')}>
              <Image style={styles.icon_menu} source={require('../resources/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 15, flex: 1 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: 'white', fontWeight: 'bold' }}>Liên hệ</Text>
          </View>
        </View>
      <View style={wrapper}>

        <View style={mapContainer}>
        <Image source={require('../resources/Map.png')} style={{ width: width - 20, height: 300 }} />
          {/* <MapView
            style={{ width: width - 20, height: 250 }}
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            <MapView.Marker
              coordinate={{ latitude: 37.78825, longitude: -122.4324 }}
              title="React Native"
              description="React Native"
            />
          </MapView> */}
        </View>
        <View style={infoContainer}>
        <View style={rowInfoContainer}>
        <Text style={lableText}>Số giấy phép: </Text>
            {/* <Image source={require('../resources/message.png')} style={imageStyle} /> */}
            <Text style={infoText}>87/GP-TTĐT, Bộ Thông tin và Truyền thông cấp ngày 27/4/2010 (hiệu lực đến ngày 26/04/2017)</Text>
          </View>
          <View style={rowInfoContainer}>
          <Text style={lableText}>Email: </Text>
            {/* <Image source={require('../resources/mail.png')} style={imageStyle} /> */}
            <Text style={infoText}>toweb@tayninh.gov.vn</Text>
          </View>
          <View style={rowInfoContainer}>
          <Text style={lableText}>Đơn vị: </Text>
            {/* <Image source={require('../resources/mail.png')} style={imageStyle} /> */}
            <Text style={infoText}>Sở Thông tin và Truyền thông Tây Ninh</Text>
          </View>
          <View style={rowInfoContainer}>
          <Text style={lableText}>Địa chỉ: </Text>
            {/* <Image source={require('../resources/location.png')} style={imageStyle} /> */}
            <Text style={infoText}>06 Trần Quốc Toản, Phường 2, Thành phố Tây Ninh, tỉnh Tây Ninh</Text>
          </View>
          <View style={[rowInfoContainer, { borderBottomWidth: 0 }]}>
            <Text style={lableText}>Điện thoại: </Text>
            {/* <Image source={require('../resources/phone.png')} style={imageStyle} /> */}
            <Text style={infoText}>(0276) 3828888</Text>
          </View>
        </View>
      </View>
      </View>
    );
  }
}

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  thanh1: {
    paddingBottom: 15,
    backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
  },
  wrapper: { flex: 1, backgroundColor: '#f3f3f3' },
  mapStyle: {
    width: width - 40,
    height: 230,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mapContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#FFFFFF',
    margin: 10,
    borderRadius: 2,
    shadowColor: '#3B5458',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2
  },
  infoContainer: {
    padding: 10,
    flex: 1,
    backgroundColor: '#FFF',
    margin: 10,
    marginTop: 0,
    borderRadius: 2,
    shadowColor: '#3B5458',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2
  },
  rowInfoContainer: {
    flex: 1,
    // flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#eb432d'
  },
  imageStyle: {
    width: 30,
    height: 30
  },
  infoText: {
    fontFamily: 'Avenir',
    color: '#AE005E',
    fontWeight: '500'
  },
  lableText: {
    fontFamily: 'Avenir',
    color: '#eb432d',
    fontWeight: '700'
  }
});
